
const About = require('../models/about')

exports.addAbout = async  (req,res) => {  
    try {
        const about = await new About(req.body)
        const result = await about.save()
        if(result) {
            res.status(201).json({
                success:true,
                data:result
            })
        }
    }
    catch(e) {
        res.status(400).json({
            success:false,
            message:"Xatolik yuz berdi"
        })
    }
}
exports.editAbout = async (req,res) => {
    try {
        const about = await About.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true})
        if(about) {
            res.status(200).json({
                success:true,
            })
        }
    }
    catch(e) {
            res.status(400).json({
                success:false,
                message:"Xatolik yuz berdi"
            })
    }
}
exports.getAbout =  async (req,res) => {
    try {
        const about = await About.find({})
        console.log(about)
        if(about) {
            res.status(200).json({
                success:true,
                data:about
            })
        }
    }
    catch(e) {
        res.status(400).json({
            success:false,
            message:"Xatolik yuz berdi"
        })
    }
}