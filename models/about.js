const mongoose = require('mongoose')

const aboutSchema = mongoose.Schema({
    about:{
        type:String
    }
}
)
module.exports = mongoose.model('about',aboutSchema)