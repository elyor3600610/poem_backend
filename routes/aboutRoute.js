const router = require('express').Router()
const {getAbout,editAbout,addAbout}  = require('../controllers/aboutController')


router.get('/api/about/get',getAbout)
router.post('/api/about/add',addAbout)
router.put('/api/about/update/:id',editAbout)

module.exports = router